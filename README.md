# Ectoplasm

A helper library for prettier console output

## Added Methods

| Type | Method | Arguments | Description |
| ---- | ------ | --------- | ----------- |
| `String` | `colored!` | _none_ | Enables colored string output |
| `string` | `white` | _none_ | Prints the text in white |
| `string` | `red` | _none_ | Prints the text in red. Alias: `error` |
| `string` | `green` | _none_ | Prints the text in green. Alias: `ok` |
| `string` | `yellow` | _none_ | Prints the text in yellow. Alias: `warn` |
| `string` | `blue` | _none_ | Prints the text in blue. Alias: `info` |
| `string` | `magenta` | _none_ | Prints the text in magenta |
| `string` | `cyan` | _none_ | Prints the text in cyan |
| `string` | `grey` | _none_ | Prints the text in grey. Alias: `dim` |
| `string` | `indent` | `amount, char: ' '` | Indents all lines of the string by the given amount with the provided character |
| `string` | `frmt` | `prefix_suffix=['<', '>']` | Named string substitution |
| `Hash` | `pretty` | `width: 25` | Outputs a `Hash` in a pretty structure |
| `Hash` | `obfuscate` | `secure_keys=['password', 'secret', 'token', 'secure']` | Obfuscates the values of a `Hash`, when the key is like the given ones |
| `Float` | `duration` | _none_ | Formats the `Float` as a duration string e.g. `1h 42m 2s` |

