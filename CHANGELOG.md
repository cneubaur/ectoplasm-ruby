### v1.2.2

#### Minor
 - Additional secure keys added for obfuscation (key, cert, pass)


### v1.2.1

#### Minor
 - Hash obfuscation fixed


### v1.2.0

#### Minor
 - `obfuscate` method added to `Hash` and `OpenStruct`
